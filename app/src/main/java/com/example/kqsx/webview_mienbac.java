package com.example.kqsx;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

public class webview_mienbac extends AppCompatActivity {

    WebView webviewKQ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview_mienbac);
        webviewKQ = (WebView) findViewById(R.id.webviuKQ);
        try {
            Intent intent = getIntent();
            String link = intent.getStringExtra("linkKQ");
//        Toast.makeText(Webview.this,link,Toast.LENGTH_LONG).show();
            webviewKQ.loadUrl(link);
            webviewKQ.setWebViewClient(new WebViewClient());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}