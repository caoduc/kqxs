package com.example.kqsx;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.HashMap;
import java.util.Map;

public class editprofile extends AppCompatActivity {
    EditText prname, premail, prphone;
    FirebaseAuth fAuth;
    FirebaseFirestore firestore;
    FirebaseUser user;
    Button save;
    StorageReference storange;

    public static final String TAG = "TAG";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editprofile);
        prname = findViewById(R.id.profileFullName);
        premail = findViewById(R.id.profileEmailAddress);
        prphone = findViewById(R.id.profilePhoneNu);
        save = findViewById(R.id.saveProfileInfo);
        fAuth = FirebaseAuth.getInstance();
        firestore = FirebaseFirestore.getInstance();
        user = fAuth.getCurrentUser();
        storange = FirebaseStorage.getInstance().getReference();

        Intent data = getIntent();
        final String fullname = data.getStringExtra("fullname");
        String email = data.getStringExtra("email");
        final String phone = data.getStringExtra("phone");

        prname.setText(fullname);
        premail.setText(email);
        prphone.setText(phone);

        Log.d(TAG,"onCreate: " + fullname + " "+ email + " " + phone);


        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(prname.getText().toString().isEmpty()||premail.getText().toString().isEmpty()||prphone.getText().toString().isEmpty()){
                    Toast.makeText(editprofile.this,"Một số chỗ chưa được điền vào",Toast.LENGTH_LONG).show();
                    return;
                }

                final String email = premail.getText().toString();
                user.updateEmail(email).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        DocumentReference docref = firestore.collection("users").document(user.getUid());
                        final Map<String,Object> edited = new HashMap<>();
                        edited.put("Email",email);
                        edited.put("Fullname",prname.getText().toString());
                        edited.put("Phone",prphone.getText().toString());
                        docref.update(edited).addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                Toast.makeText(editprofile.this,"Thông tin cá nhân đã được cập nhật",Toast.LENGTH_LONG).show();
                                startActivity(new Intent(getApplicationContext(),Profile.class));
                                finish();
                            }
                        });
                        Toast.makeText(editprofile.this,"Hoàn thành!!",Toast.LENGTH_LONG).show();

                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(editprofile.this,e.getMessage(),Toast.LENGTH_LONG).show();
                    }
                });
            }
        });


    }
}