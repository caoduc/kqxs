package com.example.kqsx;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextView;

import java.util.Random;

public class Duadoanketqua extends AppCompatActivity {

    Button btnRandom,btquaylai;
    TextView tvrandom;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_duadoanketqua);
        btnRandom = findViewById(R.id.btrandom);
        btquaylai = findViewById(R.id.btexit);
        tvrandom = findViewById(R.id.random);
        

        btnRandom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {

                    Random random = new Random();
                    int number = random.nextInt(100-0+1)+1;
                    Animation loadanima = AnimationUtils.loadAnimation(Duadoanketqua.this,R.anim.fadein);
                    tvrandom.startAnimation(loadanima);
                    tvrandom.setText(String.valueOf(number));

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
        btquaylai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {

                   startActivity(new Intent(Duadoanketqua.this, Home.class));

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }
}