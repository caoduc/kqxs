package com.example.kqsx;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.jsoup.Jsoup;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class MienBac extends AppCompatActivity {
    ListView listDS;
    ArrayList<String> arrayXS, arrayLink;
    ArrayAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mien_bac);

        listDS = findViewById(R.id.danhsach);
        arrayXS = new ArrayList<>();
        arrayLink= new ArrayList<>();
        adapter = new ArrayAdapter(this,android.R.layout.simple_list_item_1,arrayXS);
        listDS.setAdapter(adapter);


        new ReadRSS().execute("https://xskt.com.vn/rss-feed/mien-bac-xsmb.rss");
        listDS.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                try {
                    Toast.makeText(MienBac.this,arrayLink.get(position),Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(MienBac.this,webview_mienbac.class);
                    intent.putExtra("linkKQ", arrayLink.get(position));
                    startActivity(intent);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

    }
    private class ReadRSS extends AsyncTask<String,Void,String>{

        @Override
        protected String doInBackground(String... strings) {
            StringBuilder content = new StringBuilder();
            try {
                URL url = new URL (strings[0]);
                InputStreamReader inputStreamReader = new InputStreamReader(url.openConnection().getInputStream());
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String line = "";
                while ((line = bufferedReader.readLine())!=null){
                    content.append(line);
                }
                bufferedReader.close();


            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }


            return content.toString();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            try {
                XMLDOMParser parser = new XMLDOMParser();
                org.w3c.dom.Document document = (Document) parser.getDocument(s);
                NodeList nodeList = (NodeList) document.getElementsByTagName("item");
                String tieude = "";
                String kq = "";
                String link="";
                for(int i=0;i<nodeList.getLength();i++){
                    Element element = (Element) nodeList.item(i);
                    tieude = parser.getValue(element,"title");
                    kq = parser.getValue(element,"description");
                    link = parser.getValue(element,"link");
                    arrayXS.add(tieude);
//                    arrayXS.add(kq);
                    arrayLink.add(link);
                }
                adapter.notifyDataSetChanged();
//                Toast.makeText(MainActivity.this, tieude,Toast.LENGTH_LONG).show();
            } catch (Exception e) {
                e.printStackTrace();
            }


        }
    }
}