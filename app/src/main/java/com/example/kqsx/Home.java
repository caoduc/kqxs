package com.example.kqsx;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;



import android.os.Bundle;

import com.google.firebase.auth.FirebaseAuth;

public class Home extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = Home.class.getSimpleName() ;
    private CardView mncard,mbcard,mtcard,dudoancard;
    Button profile;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_home);
        mncard = findViewById(R.id.xosomn);
        mbcard = findViewById(R.id.xosomb);
        mtcard = findViewById(R.id.xosomt);
        dudoancard = findViewById(R.id.dudoankq);
        mncard.setOnClickListener(this);
        mbcard.setOnClickListener(this);
        mtcard.setOnClickListener(this);
        dudoancard.setOnClickListener(this);
        profile = findViewById(R.id.btprofile);
        profile.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        Intent i;
        try {
            switch (v.getId()){
                case R.id.xosomn : i = new Intent(this,MienNam.class ); startActivity(i); break;
                case R.id.xosomb : i = new Intent(this,MienBac.class );startActivity(i); break;
                case R.id.xosomt : i = new Intent(this,MienTrung.class );startActivity(i); break;
                case R.id.dudoankq : i = new Intent(this,Duadoanketqua.class );startActivity(i); break;
                case R.id.btprofile : i = new Intent(this,Profile.class);startActivity(i); break;
                default:break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
//    public void logout(View view){
//        FirebaseAuth.getInstance().signOut();
//        startActivity(new Intent(getApplicationContext(),dangnhap.class));
//        finish();
//    }
}