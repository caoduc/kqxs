package com.example.kqsx;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;

public class Profile extends AppCompatActivity {

    TextView pfname,pfemail,pfphone;
    Button change;
    FirebaseAuth fAuth;
    FirebaseFirestore firestore;
    String userID;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        pfname = findViewById(R.id.profileName);
        pfemail = findViewById(R.id.profileEmail);
        pfphone = findViewById(R.id.profilePhone);
        change = findViewById(R.id.changeProfile);

        fAuth = FirebaseAuth.getInstance();
        firestore = FirebaseFirestore.getInstance();
        userID = fAuth.getCurrentUser().getUid();

        DocumentReference documentReference = firestore.collection("users").document(userID);
        documentReference.addSnapshotListener(this, new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot documentSnapshot, @Nullable FirebaseFirestoreException e) {
                pfphone.setText(documentSnapshot.getString("Phone"));
                pfname.setText(documentSnapshot.getString("Fullname"));
                pfemail.setText(documentSnapshot.getString("Email"));
            }
        });





        change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               Intent i = new Intent(v.getContext(),editprofile.class);
               i.putExtra("fullname",pfname.getText().toString());
               i.putExtra("email",pfemail.getText().toString());
               i.putExtra("phone",pfphone.getText().toString());
               startActivity(i);
            }
        });

    }



    public void logout(View view){
        FirebaseAuth.getInstance().signOut();
        startActivity(new Intent(getApplicationContext(),Home.class));
        finish();
    }

    public void home(View view){
        startActivity(new Intent(getApplicationContext(),Home.class));
    }
}